# -*- coding: utf-8 -*-
"""
Created on Sat Sep  3 09:05:13 2022

 - ctrl+c:     copy matplotlib figure to clipboard
 - cpf(4):     copy figure 4 to clipboard
 
@author: birkudo
"""



#%% add ctrl+c copy figure capability
from PyQt5.QtGui import QImage
from PyQt5.QtWidgets import QApplication
import matplotlib.pyplot as plt
import io

def add_figure_to_clipboard(fig, event):
    if event.key == "ctrl+c":
       with io.BytesIO() as buffer:
            fig.savefig(buffer)
            QApplication.clipboard().setImage(QImage.fromData(buffer.getvalue()))

# function cpf = copy figure
# copy current figure to clipboard:   cpf()
# copy figure(4) to clipboard:        cpf(4)
# copy fig with win-title 'abc':      cpf('abc')
def cpf(fig=None):    # copy figure
    if fig is None:
        fig = plt.gcf()
    else:
        plt.figure(fig)
        fig = plt.gcf()
    with io.BytesIO() as buffer:
         fig.savefig(buffer)
         QApplication.clipboard().setImage(QImage.fromData(buffer.getvalue()))
    # enable ctrl+c for later use:
    fig.canvas.mpl_connect('key_press_event', lambda event: add_figure_to_clipboard(fig, event))



# %% tools for displaying question boxes in jupyter notebooks
from IPython.display import display
import ipywidgets as widgets
import functools
import urllib.parse

def urlcode(text, offset=12, encode=True):
    if encode:
        return(urllib.parse.quote(''.join([chr(ord(ch)+offset) for ch in text])))
    else:
        return(''.join([chr(ord(ch)+offset) for ch in urllib.parse.unquote(text)]))
    
def showSolution(arg,text="button has been clicked!",offset=-12,target=None):
    sol = urlcode(text, offset, encode=False)
    if target is None:
        print(sol)
    else:
        res = sol.replace('\n','<br>')
        htmlsol = f"<p style='font-family: monospace;line-height: 1.3;'>{res}</p>"
        target.value = htmlsol
    
def Question(frage, text=None):
    if text is None:
        text = frage;
        frage = 'Antwort anzeigen'
    answer = widgets.HTML(value="",
                            placeholder ='',
                            description ='',
    )
    button_download = widgets.Button(description = frage)   
    button_download.on_click(functools.partial(showSolution, text=text, target=answer))
    labels = widgets.VBox(children=[button_download, answer])
    display(labels)
    
def Question2(frage, text=None):
    if text is None:
        text = frage;
        frage = 'Antwort anzeigen'
    answer = widgets.HTML(value="",
                            placeholder ='',
                            description ='',
    )
    button_download = widgets.Button(description = frage)   
    button_download.on_click(functools.partial(showSolution, text=text))
    labels = widgets.VBox(children=[button_download, answer])
    display(labels)
    
# %%
